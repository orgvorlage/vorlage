# --------------------------------------------------------------------------------------------------
# SPDX-License-Identifier: Apache-2.0
# SPDX-FileCopyrightText: (C) 2022 Jayesh Badwaik <j.badwaik@fz-juelich.de>
# --------------------------------------------------------------------------------------------------

# --------------------------------------------------------------------------------------------------
#  Process Dev Features
# --------------------------------------------------------------------------------------------------

# --------------------------------------------------------------------------------------------------
#  Process the Warnings
# --------------------------------------------------------------------------------------------------

if(WARNING_AS_ERROR)
  target_link_libraries(vorlage::c INTERFACE tora::warning.error.c)
  target_link_libraries(vorlage::cxx INTERFACE tora::warning.error.cxx)
endif()

if(WARNING_DEPRECATED)
  target_link_libraries(vorlage::c INTERFACE tora::warning.deprecated.c)
  target_link_libraries(vorlage::cxx INTERFACE tora::warning.deprecated.cxx)
endif()

if(${WARNING_MODE} STREQUAL "none")
  target_link_libraries(vorlage::c INTERFACE tora::warning.none.c)
  target_link_libraries(vorlage::cxx INTERFACE tora::warning.none.cxx)

elseif(${WARNING_MODE} STREQUAL "essential")
  target_link_libraries(vorlage::c INTERFACE tora::warning.essential.c)
  target_link_libraries(vorlage::cxx INTERFACE tora::warning.essential.cxx)

elseif(${WARNING_MODE} STREQUAL "mandatory")
  target_link_libraries(vorlage::c INTERFACE tora::warning.mandatory.c)
  target_link_libraries(vorlage::cxx INTERFACE tora::warning.mandatory.cxx)

elseif(${WARNING_MODE} STREQUAL "recommended")
  target_link_libraries(vorlage::c INTERFACE tora::warning.recommended.c)
  target_link_libraries(vorlage::cxx INTERFACE tora::warning.recommended.cxx)

elseif(${WARNING_MODE} STREQUAL "everything")
  target_link_libraries(vorlage::c INTERFACE tora::warning.everything.c)
  target_link_libraries(vorlage::cxx INTERFACE tora::warning.everything.cxx)

endif()

# --------------------------------------------------------------------------------------------------
#  Process Dev Features
# --------------------------------------------------------------------------------------------------
if(DIAGNOSTIC_COVERAGE)
  target_link_libraries(vorlage::c INTERFACE tora::coverage.c)
  target_link_libraries(vorlage::cxx INTERFACE tora::coverage.cxx)
endif()

if(DIAGNOSTIC_ASAN)
  target_link_libraries(vorlage::c INTERFACE tora::instrument.asan.c)
  target_link_libraries(vorlage::cxx INTERFACE tora::instrument.asan.cxx)
endif()

if(DIAGNOSTIC_LSAN)
  target_link_libraries(vorlage::c INTERFACE tora::instrument.lsan.c)
  target_link_libraries(vorlage::cxx INTERFACE tora::instrument.lsan.cxx)
endif()

if(DIAGNOSTIC_UBSAN)
  target_link_libraries(vorlage::c INTERFACE tora::instrument.ubsan.c)
  target_link_libraries(vorlage::cxx INTERFACE tora::instrument.ubsan.cxx)
endif()

target_link_libraries(vorlage::c INTERFACE Threads::Threads)
target_link_libraries(vorlage::cxx INTERFACE Threads::Threads)


if(BUILD_TESTING)
  target_link_libraries(libtestvorlage PUBLIC Catch2::Catch2)
endif()
