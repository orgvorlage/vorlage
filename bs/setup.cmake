# --------------------------------------------------------------------------------------------------
# SPDX-License-Identifier: Apache-2.0
# SPDX-FileCopyrightText: (C) 2022 Jayesh Badwaik <j.badwaik@fz-juelich.de>
# --------------------------------------------------------------------------------------------------
# --------------------------------------------------------------------------------------------------
# Package to Compute Version
# --------------------------------------------------------------------------------------------------
find_package(tora REQUIRED COMPONENTS version)

# --------------------------------------------------------------------------------------------------
#  Defined Flags Target
# --------------------------------------------------------------------------------------------------
add_library(vorlage::c INTERFACE IMPORTED)
add_library(vorlage::cxx INTERFACE IMPORTED)

# --------------------------------------------------------------------------------------------------
#  Setup the Build System Scripts
# --------------------------------------------------------------------------------------------------
set(CMAKE_EXPORT_COMPILE_COMMANDS ON)
set(TORABUILD_NOPREFIX ON)
find_package(tora REQUIRED COMPONENTS torabuild)
find_package(tora REQUIRED COMPONENTS feature-set)
find_package(tora REQUIRED COMPONENTS mode)

# --------------------------------------------------------------------------------------------------
#  Define the Feature Set
# --------------------------------------------------------------------------------------------------
tora_feature_set(
  PROJECT_FEATURE
  DESCRIPTION "List of Project Features"
  VALUE ""
  DEFAULT "none"
  )

# --------------------------------------------------------------------------------------------------
#  Define the Warning Options
# --------------------------------------------------------------------------------------------------
tora_mode(
  WARNING_MODE
  DESCRIPTION "Warning Mode"
  VALUE "none;essential;mandatory;recommended;everything"
  DEFAULT "recommended"
  )

if(WARNING_AS_ERROR)
  find_package(tora REQUIRED COMPONENTS warnings.error)
endif()

if(WARNING_DEPRECATED)
  find_package(tora REQUIRED COMPONENTS warnings.deprecated)
endif()

if(${WARNING_MODE} STREQUAL "none")
  find_package(tora REQUIRED COMPONENTS warnings.none)

elseif(${WARNING_MODE} STREQUAL "essential")
  find_package(tora REQUIRED COMPONENTS warnings.essential)

elseif(${WARNING_MODE} STREQUAL "mandatory")
  find_package(tora REQUIRED COMPONENTS warnings.mandatory)

elseif(${WARNING_MODE} STREQUAL "recommended")
  find_package(tora REQUIRED COMPONENTS warnings.recommended)

elseif(${WARNING_MODE} STREQUAL "everything")
  find_package(tora REQUIRED COMPONENTS warnings.everything)

endif()

# --------------------------------------------------------------------------------------------------
# Dev : Include CTest if Testing is Enabled
# --------------------------------------------------------------------------------------------------
if(BUILD_TESTING)
  include(CTest)
endif()

# --------------------------------------------------------------------------------------------------
# Dev : Enable Surrogate Testing
# --------------------------------------------------------------------------------------------------
if(BUILD_TESTING)
  find_package(tora REQUIRED COMPONENTS surrogate)
endif()


# --------------------------------------------------------------------------------------------------
# Dev : Find Packages For Warnings
# --------------------------------------------------------------------------------------------------
if(BUILD_TESTING)
  find_package(tora REQUIRED COMPONENTS surrogate)
endif()


# --------------------------------------------------------------------------------------------------
# Dev : Setup Targets for Source Formatters
# --------------------------------------------------------------------------------------------------
if(DIAGNOSTIC_FORMAT)
  find_package(tora REQUIRED COMPONENTS clang-format)
  add_custom_target(diagnostic.format)
  add_dependencies(diagnostic.format tora.format.check)

  add_custom_target(diagnostic.format.full)
  add_dependencies(diagnostic.format.full tora.format.check.full)
endif()

# --------------------------------------------------------------------------------------------------
# Dev : Setup Targets for Linting
# --------------------------------------------------------------------------------------------------
if(DIAGNOSTIC_LINTING)
  set(TORA_CLANG_TIDY_PASSTHROUGH "-DVORLAGE_RUN_CLANG_TIDY")
  find_package(tora REQUIRED COMPONENTS clang-tidy)
  add_custom_target(diagnostic.linting)
  add_dependencies(diagnostic.linting tora.clang-tidy)
endif()

# --------------------------------------------------------------------------------------------------
# Dev : Setup Targets and Functions for Coverage
# --------------------------------------------------------------------------------------------------
if(DIAGNOSTIC_COVERAGE)
  find_package(tora REQUIRED COMPONENTS coverage)
  add_custom_target(diagnostic.coverage)
  add_dependencies(diagnostic.coverage tora.coverage)
else()
  find_package(tora REQUIRED COMPONENTS coverage.dummy)
endif()

# --------------------------------------------------------------------------------------------------
# Dev : Build Dependencies for Sanitizers
# --------------------------------------------------------------------------------------------------
if(DIAGNOSTIC_ASAN)
  find_package(tora REQUIRED COMPONENTS instrument.asan)
endif()

if(DIAGNOSTIC_LSAN)
  find_package(tora REQUIRED COMPONENTS instrument.lsan)
endif()

if(DIAGNOSTIC_UBSAN)
  find_package(tora REQUIRED COMPONENTS instrument.ubsan)
endif()

# --------------------------------------------------------------------------------------------------
# Install : Tools for Installation
# --------------------------------------------------------------------------------------------------
if(BUILD_SOURCE)

  include(CMakePackageConfigHelpers)
  include(GNUInstallDirs)

endif()




