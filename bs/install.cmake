# --------------------------------------------------------------------------------------------------
# SPDX-License-Identifier: Apache-2.0
# SPDX-FileCopyrightText: (C) 2022 Jayesh Badwaik <j.badwaik@fz-juelich.de>
# --------------------------------------------------------------------------------------------------

if(BUILD_SOURCE)
  add_library(vorlage::libvorlage ALIAS libvorlage)
  install(TARGETS libvorlage EXPORT vorlageTarget FILE_SET HEADERS)

  install(FILES ${PROJECT_BINARY_DIR}/lib/vorlageConfigVersion.cmake
    DESTINATION ${CMAKE_INSTALL_LIBDIR}/vorlage/cmake/)

  install(FILES ${PROJECT_BINARY_DIR}/lib/vorlageConfig.cmake
    DESTINATION ${CMAKE_INSTALL_LIBDIR}/vorlage/cmake)

  install(
    EXPORT vorlageTarget
    FILE vorlageTarget.cmake
    NAMESPACE vorlage::
    DESTINATION ${CMAKE_INSTALL_LIBDIR}/vorlage/cmake)
endif()


if(BUILD_DOC)
  install(DIRECTORY ${PROJECT_BINARY_DIR}/documentation
    DESTINATION ${CMAKE_INSTALL_DOCDIR})
endif()


