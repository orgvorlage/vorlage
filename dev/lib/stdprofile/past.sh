# ------------------------------------------------------------------------------
# SPDX-License-Identifier: Apache-2.0
# SPDX-FileCopyrightText: (C) 2022-2022, Jayesh Badwaik <j.badwaik@fz-juelich.de>
# ------------------------------------------------------------------------------


export STDPROFILE_C_STD=11
export STDPROFILE_CXX_STD=17
