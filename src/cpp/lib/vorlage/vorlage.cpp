// -----------------------------------------------------------------------------
// SPDX-License-Identifier: Apache-2.0
// SPDX-FileCopyrightText: (C) 2022-2022, Jayesh Badwaik <j.badwaik@fz-juelich.de>
// -----------------------------------------------------------------------------

#include <vorlage/vorlage.hpp>

namespace vorlage {
auto f(int a) -> int
{
  return 24 * a;
}

auto g(double b) -> double
{
  return 45.0 * b;
}
} // namespace vorlage
