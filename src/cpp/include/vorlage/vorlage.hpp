// -----------------------------------------------------------------------------
// SPDX-License-Identifier: Apache-2.0
// SPDX-FileCopyrightText: (C) 2020, Jayesh Badwaik (jayeshbadwaik)
// -----------------------------------------------------------------------------

#ifndef VORLAGE_VORLAGE_HPP
#define VORLAGE_VORLAGE_HPP

#include <string>

namespace vorlage {
auto f(int a) -> int;
auto g(double b) -> double;
} // namespace vorlage

#endif // VORLAGE_VORLAGE_HPP
